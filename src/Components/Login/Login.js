import React from 'react';
import './Login.css'
import logo from '../../assets/logo.png';
import {APP_URL, axiosConfig} from '../Constants';
import axios from 'axios';
import Loader from '../Loader/Loader';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUser, faKey } from '@fortawesome/free-solid-svg-icons'
class Login extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            email: '',
            password: '',
            incorrect_details: '',
            message: true,
            responseData: null,
            user_details: null,
            login_token: null,
            isLoading:false,
            redirectToReferrer: false
        };
        if(localStorage.getItem('login_token') !== null && localStorage.getItem('login_token') !=="" && localStorage.getItem('login_token') !== undefined ){
            this.props.history.push("/admin");
        }else{
            this.props.history.push("/");
        }
    }

    handleSubmit = event => {
        this.setState({
            isLoading:true
        });
        event.preventDefault();
        var data = {
            email: this.state.email,
            password: this.state.password
        }
        axios.post(APP_URL+'adminlogin',data,axiosConfig).then(function (response) {
            this.setState({
                responseData: response.data,
                message: response.data.success,
                user_details: response.data.message,
                login_token:response.data.api_token,
                isLoading:false,
                redirectToReferrer: true
            });
            try{
                if(this.state.message){
                    localStorage.setItem('login_token',this.state.login_token);
                    localStorage.setItem('email',this.state.user_details.email);
                    this.props.history.push("/admin");
                }
            }catch(e){
                console.log(e.message);
            }
        }.bind(this)).catch(function (error) {
            console.log(error);
        });
    }

    handleChange = event => {
        this.setState({
            [event.target.name]:event.target.value
        });
    }

    render(){
        
        const errorMessage = <div className="show_error">
            <span className="error_text">Incorrect Email ID or Password.</span>
        </div>
        return (
            <div className="container-fluid h-100v">
                <div className="row justify-content-center h-100 align-items-center">
                {this.state.isLoading &&
                <Loader />}
                <div className="user_card">
                    <div className="d-flex justify-content-center">
                        <div className="brand_logo_container">
                            <img src={logo} className="brand_logo" alt="Logo" />
                        </div>
                    </div>
                    <div className="d-flex justify-content-center form_container">
                        <form name="form" onSubmit={this.handleSubmit}>
                            <div className="header_login">
                                <h1>ADMIN LOGIN</h1>
                            </div>
                            {this.state.message !=true &&
                                errorMessage
                            }
                            <div className="input-group mb-3">
                                <div className="input-group-append">
                                    <span className="input-group-text"><FontAwesomeIcon icon={faUser} /></span>
                                </div>
                                <input onChange={this.handleChange} type="email" value={this.state.email} name="email" className="form-control input_user" placeholder="email" />
                            </div>
                            <div className="input-group mb-2">
                                <div className="input-group-append">
                                    <span className="input-group-text"><FontAwesomeIcon icon={faKey} /></span>
                                </div>
                                <input onChange={this.handleChange} value={this.state.password} type="password" name="password" className="form-control input_pass" placeholder="password" />
                            </div>
                            <div className="d-flex justify-content-center mt-3 login_container">
                                <button type="submit" name="button" className="btn login_btn">Login</button>
                            </div>
                        </form>
                    </div>
                    
                </div>
                </div>
            </div>
            // <div className="d-flex justify-content-center h-100 loginPageMargin">
                
            // </div>
        )
    }
}
export default Login;