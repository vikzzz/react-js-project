import React from 'react';

const ShowRecords = (props) => {
    return (
        <tr key={props.data.ID}>
            <td>{props.data.full_name}</td>
            <td>{props.data.email}</td>
            <td>{props.data.Country}</td>
            {/* <td>{props.data.Language}</td> */}
            <td>{props.data.City}</td>
            <td>{props.data.State}</td>
            <td>{props.data.Location}</td>
            <td>{props.data.TotalTimeSpend}</td>
        </tr>
    )
}
export default ShowRecords;