import React from 'react';
import {Navigation} from '../Nav/Navigation';
import {Sidebar} from '../Sidebar/Sidebar';
import axios from 'axios';
import {APP_URL,axiosConfig} from '../Constants';
import ReactPaginate from 'react-paginate';
import Loader from '../Loader/Loader';
import Search from '../TimeSpend/Search/Search';
import Section from '../Section/Section';
import ShowRecords from '../TimeSpend/ShowRecords';
class TimeSpend extends React.Component{
    constructor(props){
        super(props);
        this.state = ({
            data:[],
            selected:null,
            showMenu: false,
            searchWords:{},
            total: {},
            per_page: {},
            current_page: {},
            paginationRecords:{},
            isLoading:true,
            keyword:'',
            country:'',
            city:'',
            state:'',
            showResetButton: false
        });
    }

    showButtonHandler = (event) => {
        this.setState({
            showMenu: !this.state.showMenu
        })
    }

    getSearchKeyword = (pageNumber) => axios.get(APP_URL+'get-timedetails?keyword='+this.state.keyword+'&country='+this.state.country+'&city='+this.state.city+'&state='+this.state.state+'&page='+pageNumber,axiosConfig)
    .then(res => {
        console.log(res,"rest");
        const searchWords = res.data;
        this.setState({
            searchWords:searchWords,
            total: searchWords.result.total,
            paginationRecords:searchWords.result.last_page,
            per_page:searchWords.result.per_page,
            current_page:searchWords.result.current_page,
            isLoading: false,
            formData:null
        });
    });

    onPageChange = (data) => {
       let selected = data.selected+1;
       this.getSearchKeyword(selected);
    }

    handleSearch = (e) => {
        e.preventDefault();
        this.getSearchKeyword(1);
        console.log(this.state.keyword.length);
        if(this.state.keyword.length >0 || this.state.city.length >0 || this.state.state.length >0 || this.state.country.length >0 ){
            this.setState({
                'showResetButton': true
            });
        }else{
            this.setState({
                'showResetButton': false
            }); 
        }
    }

    resetForm = async function(e) {
        e.preventDefault();
        document.getElementById('search_form').reset();
        await this.setState({
            keyword: "",
            state: "",
            country: "",
            city: ""
        });
        this.getSearchKeyword(1);
        this.setState({'showResetButton':false});
    }

    loadChangeForKeyword = (e) => {
        this.setState({
            [e.target.name]:e.target.value
        });
    }

    UNSAFE_componentWillMount(){
        this.getSearchKeyword(1);
    }

    render(){
        let getKeywords;
        if(this.state.searchWords.message == null && this.state.searchWords.result !== undefined ){
            console.log(this.state.searchWords.result, "Inside data");
            getKeywords =<tbody>{this.state.searchWords.result ? this.state.searchWords.result.data.map((searchWord, index) => <ShowRecords data={searchWord} key={index}/>):''}</tbody>;
        } else {
            getKeywords = <tbody><tr><td colSpan="6" className="no_records_found">No Record Found.</td></tr></tbody>;
        }

        if(this.state.isLoading){
            return(
                <Loader />
            )
        }else{
            return(
                <div className={"d-flex "+(this.state.showMenu ? 'toggled': '')} id="wrapper">
                    <Sidebar showMenu={this.state}/>
                    <div id="page-content-wrapper">
                    <Navigation click={this.showButtonHandler.bind(this)}/>
                        <div className="container-fluid">
                            <Section mainText="Time Spend" smallText="Oloom" />
                            <Search
                                resetButton={this.state.showResetButton}
                                click={this.handleSearch.bind(this)} 
                                forKeyword={this.loadChangeForKeyword.bind(this)}
                                forKeyCity={this.loadChangeForKeyword.bind(this)}
                                forKeyState={this.loadChangeForKeyword.bind(this)}
                                forKeyCountry={this.loadChangeForKeyword.bind(this)}
                                resetForm={this.resetForm.bind(this)}/>
                            <div className="table-responsive-sm table-responsive-lg">
                                <table id="dtBasicExample" className="table table-striped table-bordered" cellSpacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th className="th-sm">Name</th>
                                            <th className="th-sm">Email</th>
                                            <th className="th-sm">Country</th>
                                            <th className="th-sm">City</th>
                                            <th className="th-sm">State</th>
                                            <th className="th-sm">Location</th>
                                            <th className="th-sm">Time Spend</th>
                                        </tr>
                                    </thead>{getKeywords}</table>
                            </div>
                            <ReactPaginate 
                                pageCount={parseInt(this.state.paginationRecords)}
                                pageRangeDisplayed={5}
                                marginPagesDisplayed={2}
                                pageClassName={"page-item"}
                                pageLinkClassName={"page-link"}
                                breakClassName={"page-item"}
                                activeClassName={'active'}
                                breakLinkClassName={"page-link"}
                                containerClassName={"pagination"}
                                onPageChange={this.onPageChange}
                                previousLinkClassName={"page-link"}
                                nextLinkClassName={"page-link"}/>
                        </div>
                    </div>
                </div>
            )
        }
    }
}
export default TimeSpend;