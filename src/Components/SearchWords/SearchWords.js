import React from 'react';
import {Navigation} from '../Nav/Navigation';
import {Sidebar} from '../Sidebar/Sidebar';
import axios from 'axios';
import ShowRecords from './ShowRecords';
import {APP_URL, axiosConfig} from '../Constants';
import ReactPaginate from 'react-paginate';
import Loader from '../Loader/Loader';
import Search from '../SearchWords/Search/Search';
import Section from '../Section/Section';
class SearchWord extends React.Component{

    constructor(props){
        super(props);
        this.state = ({
            data:[],
            selected:null,
            showMenu: false,
            searchWords:{},
            total: {},
            per_page: {},
            current_page: {},
            paginationRecords:{},
            isLoading:true,
            keyword:'',
            country:'',
            city:'',
            states:'',
            showResetButton: false
        });
    }

    showButtonHandler = (event) => {
        this.setState({
            showMenu: !this.state.showMenu
        })
    }

    getSearchKeyword = (pageNumber) => axios.get(APP_URL+'get-keywords?keyword='+this.state.keyword+'&country='+this.state.country+'&city='+this.state.city+'&state='+this.state.states+'&page='+pageNumber,axiosConfig)
    .then(res => {
        console.log(APP_URL+'get-keywords?keyword='+this.state.keyword+'&country='+this.state.country+'&city='+this.state.city+'&state='+this.state.states+'&page='+pageNumber,axiosConfig);
        console.log(res);
        const searchWords = res.data;
        this.setState({
            searchWords:searchWords,
            total: searchWords.result.total,
            paginationRecords:searchWords.result.last_page,
            per_page:searchWords.result.per_page,
            current_page:searchWords.result.current_page,
            isLoading: false,
            formData:null
        });
    }).catch(function (error) {
        console.log(error);
    });

    onPageChange = (data) => {
       let selected = data.selected+1;
       this.getSearchKeyword(selected);
    }

    handleSearch = (e) => {
        e.preventDefault();
        this.getSearchKeyword(1);
        console.log(this.state.keyword,"handle Search");
        if( this.state.keyword !== "" || this.state.city !== "" || this.state.states !== "" || this.state.country !== "" ){
            this.setState({
                'showResetButton': true
            });
        }else{
            this.setState({
                'showResetButton': false
            }); 
        }
    }

    resetForm = async function(e){
        e.preventDefault();
        document.getElementById('search_form').reset();
        await this.setState({
            keyword:"",
            country:"",
            city:"",
            states:""
        });
        this.getSearchKeyword(1);
        this.setState({'showResetButton':false});
    }

    loadChangeForKeyword = (e) => {
        this.setState({
            [e.target.name]:e.target.value
        });
        console.log(this.state,"state a");
    }

    componentDidMount(){
        this.getSearchKeyword(1);
    }

    render(){
        let getKeywords;
        if(this.state.searchWords.message == null){
            getKeywords =<tbody>{this.state.searchWords.result ? this.state.searchWords.result.data.map((searchWord, index) => <ShowRecords data={searchWord} key={index}/>):''}</tbody>;
        } else {
            getKeywords = <tbody><tr><td colSpan="6" className="no_records_found">No Record Found.</td></tr></tbody>;
        }

        if(this.state.isLoading){
            return(
                <Loader />
            )
        }else{
            return(
                <div className={"d-flex "+(this.state.showMenu ? 'toggled': '')} id="wrapper">
                    <Sidebar showMenu={this.state}/>
                    <div id="page-content-wrapper">
                    <Navigation click={this.showButtonHandler.bind(this)}/>
                        <div className="container-fluid">
                            <Section mainText="Search Words" smallText="Oloom" />
                            <Search
                                resetForm={this.resetForm.bind(this)}
                                resetButton={this.state.showResetButton}
                                click={this.handleSearch.bind(this)} 
                                forKeyword={this.loadChangeForKeyword.bind(this)}
                                forKeyCity={this.loadChangeForKeyword.bind(this)}
                                forKeyState={this.loadChangeForKeyword.bind(this)}
                                forKeyCountry={this.loadChangeForKeyword.bind(this)}
                                keywordVal={this.state.keyword}/>
                            <div className="table-responsive-sm table-responsive-lg">
                                <table id="dtBasicExample" className="table table-striped table-bordered" cellSpacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th className="th-sm">Name</th>
                                            <th className="th-sm">Email</th>
                                            <th className="th-sm">Keyword</th>
                                            <th className="th-sm">Country</th>
                                            <th className="th-sm">City</th>
                                            <th className="th-sm">State</th>
                                            <th className="th-sm">Search Time</th>
                                        </tr>
                                    </thead>{getKeywords}</table>
                            </div>
                            <ReactPaginate 
                                pageCount={parseInt(this.state.paginationRecords)}
                                pageRangeDisplayed={5}
                                marginPagesDisplayed={2}
                                pageClassName={"page-item"}
                                pageLinkClassName={"page-link"}
                                breakClassName={"page-item"}
                                activeClassName={'active'}
                                breakLinkClassName={"page-link"}
                                containerClassName={"pagination"}
                                onPageChange={this.onPageChange}
                                previousLinkClassName={"page-link"}
                                nextLinkClassName={"page-link"}/>
                        </div>
                    </div>
                </div>
            )
        }
    }
}
export default SearchWord;