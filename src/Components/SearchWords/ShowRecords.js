import React from 'react';

const ShowRecords = (props) => {
    return (
        <tr key={props.data.ID}>
            <td>{props.data.full_name}</td>
            <td>{props.data.email}</td>
            <td>{props.data.Keyword}</td>
            <td>{props.data.country}</td>
            <td>{props.data.city}</td>
            <td>{props.data.state}</td>
            <td>{props.data.CreatedAt}</td>
        </tr>
    )
}
export default ShowRecords;