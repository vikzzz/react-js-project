import React from 'react';

export default class Search extends React.Component{
    constructor(props){
        super(props);
    }
    
    render(){
        console.log(this.props.resetButton,"childer");
        return(
            <form name="search_form" id="search_form">
                <div className="row">
                    <div className="col-md-2">
                        <input type="text" value={this.props.keywordVal} name="keyword" onChange={(e) => this.props.forKeyword(e)} className="form-control" placeholder="Enter Keyword" />
                    </div>
                    <div className="col-md-2">
                        <input type="text" name="country" onChange={(e) => this.props.forKeyword(e)} className="form-control" placeholder="Enter Country" />
                    </div>
                    <div className="col-md-2">
                        <input type="text" name="states" onChange={(e) => this.props.forKeyword(e)} className="form-control" placeholder="Enter State" />
                    </div>
                    <div className="col-md-2">
                        <input type="text" name="city" onChange={(e) => this.props.forKeyword(e)} className="form-control" placeholder="Enter City" />
                    </div>
                    <div className="col-md-2">
                        <button type="submit" onClick={(e) => this.props.click(e)} className="btn btn-primary">Search</button>
                        {this.props.resetButton &&
                            <button type="submit" onClick={this.props.resetForm} className="btn btn-primary reset_button">Reset</button>
                        }
                    </div>
                </div>
            </form>
        )
    }

}