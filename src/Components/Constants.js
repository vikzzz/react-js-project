export const APP_URL = "https://st4.idsil.com/oloom/public/";
export const Pagination_Limit = 2;
export const API_TOKEN="rt5fdase";
export const axiosConfig = {
    headers: {
        'Content-Type': 'application/json;charset=UTF-8',
        "Access-Control-Allow-Origin": "*",
        "APITOKEN": API_TOKEN
    }
};