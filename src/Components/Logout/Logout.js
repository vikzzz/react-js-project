import React from 'react';
import {Redirect} from 'react-router-dom';

class Logout extends React.Component{
    constructor(props){
        super(props);
        localStorage.removeItem('login_token');
        localStorage.removeItem('email');
    }

    render(){
        return(
            <Redirect to="/" />
        )
    }
}

export default Logout;