import React from 'react';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faBars, faKey } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {Link} from 'react-router-dom';
library.add(faBars, faKey);
export const Navigation = ( props ) => {
    const fontawesomeSize = {
        fontSize:'23px'
    }
    return (
        <nav className="navbar navbar-expand-lg justify-content-between align-items-center">
            <FontAwesomeIcon style={fontawesomeSize} onClick={props.click} data-toggle="collapse" data-target="#navbarSupportedContent" icon="bars" color="#fff"/>
            <div className="logout_button" id="button-3">
                <div id="circle"></div>
                <Link to={'/admin/logout'}>Logout</Link>
            </div>
        </nav>
    )
}