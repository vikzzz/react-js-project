import React from 'react';
import {Navigation} from '../Nav/Navigation';
import {Sidebar} from '../Sidebar/Sidebar';
import Section from '../Section/Section';
import axios from 'axios';
import {APP_URL, axiosConfig} from '../Constants';
import Loader from '../Loader/Loader';
class ChangePassword extends React.Component{
    constructor(props){
        super(props);
        this.state = ({
            showMenu: false,
            old_password: null,
            new_password: null,
            confirm_password: null,
            veOldPass: null,
            veNewPass: null,
            veConfPass: null,
            status: null,
            message: null,
            isLoading:false
        });
    }

    showButtonHandler = (event) => {
        this.setState({
            showMenu: !this.state.showMenu
        })
    }

    changePassword = (event) => {
        event.preventDefault();
        if(this.state.old_password == null){
            this.setState({
                veOldPass: "Please Enter Old Password."
            });
        }else{
            this.setState({
                veOldPass: null,
                veOldPassmsg: true
            });
        }
        if(this.state.new_password == null){
            this.setState({
                veNewPass: "Please Enter New Password."
            });
        }else{
            this.setState({
                veNewPass: null,
                veNewPassmsg: true
            });
        }

        if(this.state.confirm_password !== this.state.new_password && this.state.new_password !== null){
            this.setState({
                veConfPass: "Confirm Password doesn't match."
            });
        }else{
            this.setState({
                veConfPass:null,
                veConfPassmsg: true
            });
        }
        console.log(this.state);
        if(this.state.veOldPassmsg && this.state.veNewPassmsg ){
            this.setState({
                isLoading:true
            });
            event.preventDefault();
            axios.post(APP_URL+'change-password', {
                old_password: this.state.old_password,
                new_password: this.state.new_password,
                email: localStorage.getItem('email')
            },axiosConfig).then(function (response) {
                console.log(response,"This is response");
                this.setState({
                    status: response.data.success,
                    message:response.data.message,
                    isLoading:false
                });
                try{
                    if(this.state.status){
                        document.getElementById('change_passwords').reset();
                        this.setState({
                            old_password: null,
                            new_password: null,
                            confirm_password: null,
                        })
                    }
                }catch(e){
                    console.log(e.message);
                }
            }.bind(this)).catch(function (error) {
                console.log(error);
            });
        }
    }

    handleInput = (event) => {
        this.setState({
            [event.target.name]:event.target.value
        });
    }

    render(){        
        const buttonStyle={
            margin:'25px auto',
            display: 'block'
        }
        let successMessage;
        successMessage = <div>{this.state.status && <div className="show_error"><div className="success_text">Password Changed Successfully.</div></div>}</div>
        let errorMessage;
            errorMessage = <div>
                {this.state.veOldPass !== null && <div className="show_error"><div className="error_text">{this.state.veOldPass}</div></div> }
                {this.state.veNewPass !== null && <div className="show_error"><div className="error_text">{this.state.veNewPass}</div></div> }
                {this.state.veConfPass !== null && <div className="show_error"><div className="error_text">{this.state.veConfPass}</div> </div>}
                {this.state.status === false && <div className="show_error"><div className="error_text">{this.state.message}</div></div> }</div>
            
        return(
            <div className={"d-flex "+(this.state.showMenu ? 'toggled': '')} id="wrapper">
                <Sidebar showMenu={this.state}/>
                <div id="page-content-wrapper">
                <Navigation click={this.showButtonHandler.bind(this)}/>
                    <div className="container-fluid">
                        <Section mainText="Change Password" smallText="Oloom" />
                            <div className="row">
                                <div className="col-md-5">
                                        {errorMessage}
                                        {successMessage}
                                </div>
                            </div>
                        <form id="change_passwords" name="change_password" method="post" onSubmit={this.changePassword}>
                            <div className="row">
                                <div className="col-md-5 col-sm-12">
                                {this.state.isLoading &&
                                    <Loader />}
                                    <label>Old Password</label>
                                    <input type="password" name="old_password" onChange={this.handleInput} className="form-control" />
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-5 col-sm-12">
                                    <label>New Password</label>
                                    <input type="password" name="new_password" onChange={this.handleInput} className="form-control" />
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-5 col-sm-12">
                                    <label>Confirm Password</label>
                                    <input type="password" name="confirm_password" onChange={this.handleInput} className="form-control" />
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-5 col-sm-12">
                                    <button style={buttonStyle} type="submit" name="change_password" className="btn btn-primary">Change Password</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        )
    }

}

export default ChangePassword;