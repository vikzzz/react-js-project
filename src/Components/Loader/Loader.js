import React from 'react';
import '../Loader/Loader.css';
import loader from '../../assets/loader.gif';

export default class Loader extends React.Component{
    render(){
        return (
            <div id="loader-wrapper">
                <div id="loader"><img src={loader} /></div>
            </div>
        )
    }
}