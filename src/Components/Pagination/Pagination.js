import React from 'react';
import { Link } from 'react-router-dom';
const PagesCount = props => {
    let current_page = props.currentPage;
    let pageNumbers = [];
    if (props.totalPages !== null){
        for (let i = 1; i <= Math.ceil(props.totalPages / props.perPage); i++) {
            pageNumbers.push(i);
        }
    }
    console.log(pageNumbers);
    const paging = pageNumbers.map((number,index) => 
        <li className="page-item" key={index}><Link to={'/'} onClick={e => props.callBackFromParent(number)} key={index} className="page-link">{number}</Link></li>
    );
    
    return(
        <div>
            <nav aria-label="Page navigation example">
                <ul className="pagination">
                    {paging}
                </ul>
            </nav>
        </div>
    )
}

// const Pagination = props => {
//     console.log(props,"props data");
//     return(
//         <nav aria-label="Page navigation example">
//             <ul className="pagination">
//                 <li onClick="" className="page-item"><a className="page-link" href="#">Previous</a></li>
//                 <li className="page-item"><a className="page-link" href="#">1</a></li>
//                 <li className="page-item"><a className="page-link" href="#">2</a></li>
//                 <li className="page-item"><a className="page-link" href="#">3</a></li>
//                 <li className="page-item"><a className="page-link" href="#">Next</a></li>
//             </ul>
//         </nav>
//     )
// }

export {PagesCount};