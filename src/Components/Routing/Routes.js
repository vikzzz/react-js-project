import React from 'react'
import { Route, BrowserRouter as Router, Redirect } from 'react-router-dom'
import SearchWords from '../SearchWords/SearchWords';
import TimeSpend from '../TimeSpend/TimeSpend';
import Languages from '../Languages/Languages';
import Login from '../Login/Login';
import ChangePassword from '../ChangePassword/ChangePassword';
import Logout from '../Logout/Logout';


const ProtectedRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={(props) => (
    localStorage.getItem('login_token')!==null
      ? <Component {...props} />
      : <Redirect to={{
          pathname: '/login',
          state: { from: props.location }
        }} />
  )} />
);


export const Routing = () => (
  <Router>
    <div>
      <Route exact path="/" component={Login} />
      <Route exact path="/login" component={Login} />
      <ProtectedRoute exact path="/admin" component={SearchWords} />
      <ProtectedRoute path="/admin/search-words" component={SearchWords} />
      <ProtectedRoute path="/admin/time-spend" component={TimeSpend} />
      <ProtectedRoute path="/admin/languages" component={Languages} />
      <ProtectedRoute path="/admin/change-password" component={ChangePassword} />
      <ProtectedRoute exact path="/admin/logout" component={Logout} />
    </div>
  </Router>
)