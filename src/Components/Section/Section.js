import React from 'react';
import '../Sidebar/Sidebar.css'
import { Link } from 'react-router-dom';
import '../Section/Section.css';
export const Section = (props) =>{
    return (
        <section className="content-header">
            <h1>
                {props.mainText}
                <small>{props.smallText}</small>
            </h1>
        </section>
    )
}
export default Section;