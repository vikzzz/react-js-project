import React from 'react';
import '../Sidebar/Sidebar.css'
import { Link } from 'react-router-dom';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faBars, faFileWord, faCalendarTimes, faLanguage, faExchangeAlt } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
library.add(faBars, faFileWord, faCalendarTimes, faLanguage, faExchangeAlt);
export const Sidebar = (props) =>{
    return (
        <div className="bg-light border-right" id="sidebar-wrapper">
            <div className="sidebar-heading"><Link to="/admin">OLOOM</Link></div>
            <div className="list-group list-group-flush">
                <Link to={'/admin'} className="list-group-item list-group-item-action"><FontAwesomeIcon icon="bars" color="#1c362e"/>&nbsp;&nbsp;Dashboard</Link>
                <Link to={'/admin/search-words'} className="list-group-item list-group-item-action"><FontAwesomeIcon icon="file-word" color="#1c362e"/>&nbsp;&nbsp;Search Words</Link>
                <Link to={'/admin/time-spend'} className="list-group-item list-group-item-action"><FontAwesomeIcon icon="calendar-times" color="#1c362e"/>&nbsp;&nbsp;Time Spend</Link>
                <Link to={'/admin/languages'} className="list-group-item list-group-item-action"><FontAwesomeIcon icon="language" color="#1c362e"/>&nbsp;&nbsp;Languages</Link>
                <Link to={'/admin/change-password'} className="list-group-item list-group-item-action"><FontAwesomeIcon icon="exchange-alt" color="#1c362e"/>&nbsp;&nbsp;Change Password</Link>
            </div>
        </div>
    )
}