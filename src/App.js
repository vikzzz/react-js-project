import React from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="container">
      <div className="row">
          <div className="col-md-6">
            Test 1
          </div>
          <div className="col-md-6">
            Test 2
          </div>
      </div>
    </div>
  );
}

export default App;
